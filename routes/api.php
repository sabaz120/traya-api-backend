<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('signup', 'Api\AuthController@register');
Route::post('login', 'Api\AuthController@login');
Route::middleware('jwt.refresh')->get('/token/refresh', 'Api\AuthController@refresh');
Route::group(['prefix' => 'auth', 'middleware' => 'jwt.auth'], function () {
  //Retorna datos del usuario autenticado
  Route::get('user', 'Api\AuthController@user');
  //Actualizar datos de usuario / perfil
  Route::post('user/update', 'Api\AuthController@update');

  //Cerrar sesión
  Route::post('logout', 'Api\AuthController@logout');

});
//Obtener servicios
Route::get('services', 'Api\ServicesController@index');
//Obtener usuarios asociados a un servicio
Route::get('services_user', 'Api\ServicesUserController@users');
//Obtener contrataciones
Route::get('hiring', 'Api\HiringsController@index');
//Ver resumen de una contratación
Route::get('hiring/{id}', 'Api\HiringsController@show');
//Ver notificaciones de un usuario
Route::get('notification/{id}', 'Api\NotificationController@getNotifications');
Route::group(['middleware'=>'jwt.auth'], function () {
  //Crear contratacion
  Route::post('hiring', 'Api\HiringsController@store');
  //Crear contratacion
  Route::post('hiring_mt4', 'Api\HiringsController@mt4');
  //Actualizar estado contratacion
  Route::put('hiring', 'Api\HiringsController@update');
  //Actualizar estado de notificación
  Route::put('notification/{id}', 'Api\NotificationController@markRead');

  //Crear servicio
  Route::post('services', 'Api\ServicesController@store');
  //Asociar servicio a usuario autenticado
  Route::post('services_user', 'Api\ServicesUserController@store');
  ;
});//middle auth
