<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;
use App\Service;
use App\Http\Requests\CreateServiceRequest;
class ServicesController extends BaseApiController
{

  public function index(Request $request){
    try {
      $parameters=$this->parametersUrl();
      $services=Service::query();
      $filters=isset($request->filters) ? json_decode($request->filters) : (object)[];
      if(isset($filters->name))
        $services->where('name', 'like', "%$filters->name%");
      $services=$services->get();
      foreach($services as $service){
        if($service->logo){
          $service->logo=url($service->logo);
        }
      }//foreach
      $response=[
        'data'=>$services
      ];
    } catch (\Exception $e) {
      //Message Error
      $status = 500;
      $response = [
        'errors' => $e->getMessage()
      ];
    }
    return response()->json($response, $status ?? 200);
  }//index()

  public function store(Request $request){
    try {
      $this->validateRequestApi(new CreateServiceRequest($request->all()));
      $logo=null;
      if(isset($request->logo) && $request->logo){
        $logo=$request->logo;
        unset($request['logo']);
      }
      $service=Service::create($request->all());
      if($logo){
        $service->logo=saveImage($logo,'services/'.$service->id.'.jpg');
        $service->update();
      }
      $response=[
        'data'=>$service,
        'msg'=>'Servicio creado satisfactoriamente'
      ];
    } catch (\Exception $e) {
      //Message Error
      $status = 500;
      $response = [
        'errors' => $e->getMessage()
      ];
    }
    return response()->json($response, $status ?? 200);

  }//store
}
