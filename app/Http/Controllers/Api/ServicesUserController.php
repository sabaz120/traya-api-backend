<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;
use App\ServicesUser;
use App\Hiring;
use App\Http\Requests\CreateServicesUserRequest;
use App\Http\Requests\GetServicesUserRequest;
use DB;
use App\Models\BackpackUser as User;
use Auth;
class ServicesUserController extends BaseApiController
{
  public function store(Request $request){
    try {
      DB::beginTransaction();
      $data=$request->all();
      $user=Auth::guard('api')->user() ? Auth::guard('api')->user() : Auth::user();
      foreach($data['services'] as $service){
        // $this->validateRequestApi(new CreateServicesUserRequest($service));
        ServicesUser::create([
          'service_id'=>$service,
          'user_id'=>$user->id
        ]);
      }//foreach
      $response=[
        'data'=>'',
        'msg'=>'Servicio(s) asociados correctamente'
      ];
      DB::commit();
    } catch (\Exception $e) {
      //Message Error
      DB::rollBack();
      $status = 500;
      $response = [
        'errors' => $e->getMessage()
      ];
    }
    return response()->json($response, $status ?? 200);

  }//store

  public function users(Request $request){
    try {
      $data=$request->all();

      if(isset($data['services']))
      is_array($data['services']) ? true : $data['services'] = [$data['services']];

      $this->validateRequestApi(new GetServicesUserRequest($data));

      //Get users of service
      $usr=\Backpack\Base\app\Models\BackpackUser::query();
      $usr->whereHas('services', function ($query) use ($data) {
        $query->whereIn('service_id',$data['services']);
      });
      $filters=isset($request->filters) ? json_decode($request->filters) : (object)[];
      if(isset($filters->name))
        $usr->where('name', 'like', "%$filters->name%");
      $usr->orderBy('created_at','ASC');
      $usr=$usr->with(['profile'])->get();
      foreach($usr as &$us){
        if($us->profile->image)
          $us['image']=url($us->profile->image);
        $us['completed_services']=Hiring::where('bidder_id',$us->id)->whereIn('service_id',$data['services'])->where('status_id',3)->count();
        $us['averageRating']=$us->averageRating;
        $us['averageRatingFloat']=(float)$us->averageRating;
        $us['averageRatingInt']=(int)$us->averageRating;
        $us['ratingPercent']=$us->ratingPercent(5);
        $comments=Hiring::where('bidder_id',$us->id)->where('status_id',4)->with('latestHistory')->get();
        $arrayComments=[];
        foreach($comments as $com){
          $arrayComments[]=[
            'userName'=>$com->latestHistory->user->name,
            'userEmail'=>$com->latestHistory->user->email,
            'comment'=>$com->latestHistory->comment,
            'created_at_date'=>$com->latestHistory->created_at->format('d-m-Y'),
            'created_at_time'=>$com->latestHistory->created_at->format('H:i:s'),
          ];
        }
        $us['comments']=$arrayComments;
      }


      $response=['data'=>$usr];
    } catch (\Exception $e) {
      $status = 500;
      $response = [
        'errors' => $e->getMessage()
      ];
    }//catch
    return response()->json($response, $status ?? 200);
  }//users()
}
