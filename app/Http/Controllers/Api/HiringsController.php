<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;
use Auth;
use App\Hiring;
use App\Profile;
use App\HiringHistory;
use App\Notification;
use App\Http\Requests\CreateHiringRequest;
use App\Http\Requests\CreateMt4HiringRequest;
use App\Http\Requests\UpdateHiringRequest;
use App\Models\BackpackUser as User;
use App\Status;

class HiringsController extends BaseApiController
{

    public function hiringTransformer($data){
      $hirings=[];
      foreach($data as $hiring){
        $historyStatus=[];
        $timeLastHistory="";
        foreach($hiring->history as $history){
          $historyStatus[]=[
            'user'=>$history->user->name,
            'status'=>$history->status->name,
            'comment'=>$history->comment,
            'created_at' =>$history->created_at->format('d-m-Y'),
            'created_at_time' =>$history->created_at->format('H:i:s')
          ];
          $dateLastHistory=$history->created_at->format('d-m-Y');
          $timeLastHistory=$history->created_at->format('H:i:s');
        }//foreach history statuses
        $logo=null;
        if($hiring->service->logo)
          $logo=url($hiring->service->logo);
        $hirings[]=[
          'id'=>$hiring->id,
          'applicant'=>[
            'id'=>$hiring->applicant->id,
            'name'=>$hiring->applicant->name,
            'phone'=>$hiring->applicant->profile->phone,
            'description'=>$hiring->applicant->profile->description,
            'image'=>url($hiring->applicant->profile->image),
            'last_sesion'=>$hiring->applicant->last_login
          ],
          'bidder'=>[
            'id'=>$hiring->bidder->id,
            'name'=>$hiring->bidder->name,
            'phone'=>$hiring->bidder->profile->phone,
            'description'=>$hiring->bidder->profile->description,
            'image'=>url($hiring->bidder->profile->image),
            'last_sesion'=>$hiring->bidder->last_login
          ],
          'service'=>$hiring->service->name,
          'service_logo'=>$logo,
          'description'=>$hiring->description,
          'status'=>$hiring->status->name,
          'description'=>$hiring->description,
          'history'=>$historyStatus,
          'date_last_history'=>$dateLastHistory,
          'time_last_history'=>$timeLastHistory
        ];
      }//hiring
      return $hirings;
    }//hiringTransformer()
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      try {
        if(!isset($_GET['user_id']))
          throw new \Exception(json_encode(['User id is required']),401);
        $user_id=$_GET['user_id'];
        $hirings=Hiring::query();
        $user=User::find($user_id);
        if($user->hasRole('Demandante'))
          $hirings->where('applicant_id',$user_id);
        else
          $hirings->where('bidder_id',$user_id);
        $filters=isset($request->filters) ? json_decode($request->filters) : (object)[];
        if(isset($filters->status_id)){
          is_array($filters->status_id) ? true : $filters->status_id = [$filters->status_id];
          $hirings->whereIn('status_id',$filters->status_id);
        }
        $hirings->orderBy('created_at','DESC');
        $hirings=$hirings->get();
        $hirings=$this->hiringTransformer($hirings);
        $response=[
          'data'=>$hirings
        ];
      } catch (\Exception $e) {
        //Message Error
        $status = 500;
        $response = [
          'errors' => $e->getMessage()
        ];
      }
      return response()->json($response, $status ?? 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      try {
        $data=$request->all();
        $this->validateRequestApi(new CreateHiringRequest($data));
        $user=Auth::guard('api')->user() ? Auth::guard('api')->user() : Auth::user();
        $data['applicant_id']=$user->id;
        $data['status_id']=1;//En espera de ok.
        $hiring=Hiring::create($data);
        $hiringHistory=HiringHistory::create([
          'hiring_id'=>$hiring->id,
          'status_id'=>1,
          'user_id'=>$user->id
        ]);
        //Notificación a usuario trabajador que tiene una solicitud de servicio.
        Notification::create([
          'user_id'=>$request->bidder_id,
          'text'=>'Tienes una solicitud de empleo de '.$hiring->applicant->name.' en el servicio '.$hiring->service->name.' pendiente por aceptar/rechazar.'
        ]);
        $response=[
          'msg'=>'Contratación creada exitosamente'
        ];
      } catch (\Exception $e) {
        //Message Error
        $status = 500;
        $response = [
          'errors' => $e->getMessage()
        ];
      }
      return response()->json($response, $status ?? 200);
    }

    /**
     * MT 4 Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function mt4(Request $request){
       try {
         $data=$request->all();
         $this->validateRequestApi(new CreateMt4HiringRequest($data));
         $user=Auth::guard('api')->user() ? Auth::guard('api')->user() : Auth::user();
         $users=User::query();
         $users->whereHas('services', function ($query) use ($data) {
           $query->where('service_id',$data['service_id']);
         });
         $users->orderBy('created_at','ASC');
         $users->whereIn('users.id', function($query) use($data){
           $query->select('user_id')
           ->from('services_users')
           ->where('service_id',$data['service_id']);
         });
         $users->limit(4);
         $users=$users->get();
         foreach($users as $usr){
           $hiring=Hiring::create([
             'bidder_id'=>$usr->id,
             'applicant_id'=>$user->id,
             'status_id'=>1,
             'service_id'=>$request->service_id
           ]);
           $hiringHistory=HiringHistory::create([
             'hiring_id'=>$hiring->id,
             'status_id'=>1,
             'user_id'=>$user->id
           ]);
           //Notificación a usuario trabajador que tiene una solicitud de servicio.
           Notification::create([
             'user_id'=>$usr->id,
             'text'=>'Tienes una solicitud de servicio '.$hiring->service->name.' pendiente por aceptar/rechazar.'
           ]);
         }
         $response=[
           'msg'=>'Contrataciones creadas exitosamente'
         ];
       } catch (\Exception $e) {
         //Message Error
         $status = 500;
         $response = [
           'errors' => $e->getMessage()
         ];
       }
       return response()->json($response, $status ?? 200);

     }//mt4

    /**
     * Update resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      try {
        $data=$request->all();
        $this->validateRequestApi(new UpdateHiringRequest($data));
        $user=Auth::guard('api')->user() ? Auth::guard('api')->user() : Auth::user();
        $hiring=Hiring::where('id',$request->hiring_id)->first();
        $status=$hiring->status_id;
        if($status==5 || $status==4)
          throw new \Exception('Esta contratación ya ha sido cerrada.');
        $hiring=Hiring::where('id',$request->hiring_id)->update(['status_id'=>$request->status_id]);
        $hiring=Hiring::where('id',$request->hiring_id)->first();
        $hiringHistory=HiringHistory::create([
          'hiring_id'=>$request->hiring_id,
          'status_id'=>$request->status_id,
          'user_id'=>$user->id,
          'comment'=>isset($request->comment) ? $request->comment : ''
        ]);

        if(isset($request->calification) && isset($request->bidder_id)){
          $bidder=\Backpack\Base\app\Models\BackpackUser::find($request->bidder_id);
          $rating = new \willvincent\Rateable\Rating;
          $rating->rating = $request->calification;
          $rating->user_id = $user->id;
          // $rating->user_id = $request->bidder_id;
          $bidder->ratings()->save($rating);
          // $profile=Profile::where('id',$user->profile->id)->first();
          // if($request->calification==1 || $request->calification=="1"){
          //   $calification=(int)$user->profile->positive_calification+1;
          //   $profile->positive_calification=$calification;
          // }else{
          //   $calification=(int)$user->profile->negative_calification+1;
          //   $profile->negative_calification=$calification;
          // }
          // $profile->update();
        }


        $response=[
          'msg'=>'Estado de contratación actualizado exitosamente',
          'hiring'=>$hiring
        ];
        $status=(int)200;
      } catch (\Exception $e) {
        //Message Error
        $status = 500;
        $response = [
          'errors' => $e->getMessage()
        ];
      }
      return response()->json($response, $status ?? 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
          $hiring=Hiring::findOrFail($id);
          $historyStatus=[];
          foreach($hiring->history as $history){
            $historyStatus[]=[
              'user'=>$history->user->name,
              'status'=>$history->status->name,
              'status_id'=>$history->status_id,
              'comment'=>$history->comment,
              'created_at'=>$history->created_at->format('d-m-Y'),
              'created_at_time' =>$history->created_at->format('H:i:s')
            ];
          }//foreach history statuses
          $response=[
            'id'=>$id,
            'applicant'=>[
              'id'=>$hiring->applicant->id,
              'name'=>$hiring->applicant->name,
              'phone'=>$hiring->applicant->profile->phone,
              'description'=>$hiring->applicant->profile->description,
              'image'=>url($hiring->applicant->profile->image),
            ],
            'bidder'=>[
              'id'=>$hiring->bidder->id,
              'name'=>$hiring->bidder->name,
              'phone'=>$hiring->bidder->profile->phone,
              'description'=>$hiring->bidder->profile->description,
              'image'=>url($hiring->bidder->profile->image),
            ],
            'service'=>$hiring->service->name,
            'status'=>$hiring->status->name,
            'description'=>$hiring->description,
            'status_id'=>$hiring->status_id,
            'history'=>$historyStatus,
            'statusesHirings'=>Status::all()
          ];
        } catch (\Exception $e) {
          //Message Error
          $status = 500;
          $response = [
            'errors' => $e->getMessage()
          ];
        }
        return response()->json($response, $status ?? 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
