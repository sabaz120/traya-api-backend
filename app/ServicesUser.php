<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesUser extends Model
{
  protected $table="services_users";

  protected $fillable = [
    'user_id',
    'service_id',
  ];
}
